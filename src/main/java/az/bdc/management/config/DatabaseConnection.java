package az.bdc.management.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    public static Connection connect() throws SQLException {
        String url = "jdbc:mysql://localhost:5340/market-management";
        String username = "root";
        String password = "123456";
        System.out.println("Hello and welcome!");
        return DriverManager.getConnection(url,username,password);
    }
}
