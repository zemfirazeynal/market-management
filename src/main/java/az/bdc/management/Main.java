package az.bdc.management;

import az.bdc.management.dao.Product;
import az.bdc.management.service.ProductService;
import az.bdc.management.service.ProductServiceImpl;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
//        Product product = new Product();
//        product.setProductName("coca cola");
//        product.setType("su");
//        product.setAmount(1.80f);
//        product.setBarCode("46588900");
//        product.setCount(14);


        ProductService productService = new ProductServiceImpl();
        Product updatedUser = productService.getById(5);
        updatedUser.setProductName("maye sabun");
        productService.update(updatedUser);
        System.out.println(updatedUser);

    }
}

