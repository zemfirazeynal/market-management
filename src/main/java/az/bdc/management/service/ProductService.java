package az.bdc.management.service;

import az.bdc.management.dao.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll();

    boolean update(Product product);

    boolean delete(int id);

    boolean insert(Product p);

    Product getById(int id);
}
