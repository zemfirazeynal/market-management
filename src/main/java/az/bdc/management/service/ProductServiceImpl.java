package az.bdc.management.service;

import az.bdc.management.config.DatabaseConnection;
import az.bdc.management.dao.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        try (Connection connection = DatabaseConnection.connect();
             Statement statement = connection.createStatement()) {
            statement.execute("select * from product");
            ResultSet resultSet = statement.getResultSet();

            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String productName = resultSet.getString(2);
                String type = resultSet.getString(3);
                float amount = resultSet.getFloat(4);
                String barCode = resultSet.getString(5);
                int count = resultSet.getInt(6);

                Product product = new Product(id, productName, type, amount, barCode, count);
                products.add(product);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return products;
    }


    @Override
    public boolean update(Product product) {
        List<Product> products = new ArrayList<>();
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement("update product set product_Name=?, type=?, amount=?, bar_Code=?, count=? where id=?")){

            preparedStatement.setInt(1, product.getId());
            preparedStatement.setString(2,product.getProductName());
            preparedStatement.setString(3, product.getType());
            preparedStatement.setFloat(4, product.getAmount());
            preparedStatement.setString(5, product.getBarCode());
            preparedStatement.setInt(6, product.getCount());
           // preparedStatement.setInt(6, product.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean insert(Product product) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("insert into product (id,product_Name, type, amount,bar_Code, count) values (?,?,?,?,?,?)")) {
            preparedStatement.setInt(1, product.getId());
            preparedStatement.setString(2, product.getProductName());
            preparedStatement.setString(3, product.getType());
            preparedStatement.setFloat(4, product.getAmount());
            preparedStatement.setString(5, product.getBarCode());
            preparedStatement.setInt(6, product.getCount());
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(int id) {

        try(Connection connection = DatabaseConnection.connect();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("delete from product where id=?")){
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            preparedStatement.getResultSet();
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Product getById(int id) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("select * from product where id=?")) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            Product product = null;
            while (resultSet.next()) {
                 product = new Product(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getFloat(4),
                        resultSet.getString(5),
                        resultSet.getInt(6));
            }
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

