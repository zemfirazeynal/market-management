package az.bdc.management.dao;

public class Product {
    private int id;
    private String productName;
    private String type;
    private float amount;
    private String barCode;
    private int count;

    public Product() {
    }

    public Product(int id, String productName, String type, float amount, String barCode, int count) {
        this.id = id;
        this.productName = productName;
        this.type = type;
        this.amount = amount;
        this.barCode = barCode;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getType() {
        return type;
    }

    public float getAmount() {
        return amount;
    }

    public String getBarCode() {
        return barCode;
    }

    public int getCount() {
        return count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", barCode='" + barCode + '\'' +
                ", count=" + count +
                '}';
    }
}
